const readline = require("readline");
const rl = readline.createInterface({
	input: process.stdin,
	output: process.stdout,
});

const input1 = (label) => {
	return new Promise((resolve) => {
		rl.question(`Masukkan ${label} : `, (answer) => {
			resolve(Number(answer));
		});
	});
};

const input2 = (label) => {
	return new Promise((resolve) => {
		rl.question(`Masukkan ${label} : `, (answer) => {
			resolve(Number(answer));
		});
	});
};

const inOperator = (label) => {
	return new Promise((resolve) => {
		rl.question(`Masukkan operator : `, (answer) => {
			resolve(answer);
		});
	});
};

const main = () => {
	console.log("Pilih kalkulasi yang ingin dilakukan:");
    console.log("1. Aritmatika Dasar\n2. Akar Kuadrat\n3. Luas Persegi\n4. Volume Kubus\n5. Volume Tabung\n6. EXIT");
    rl.question("Masukkan: ", async (answer) =>{
        let num1, num2, hasil = 0;
        switch(answer){
            case "1":
                num1 = await input1("bilangan pertama");
                operator = await inOperator();
                num2 = await input2("bilangan kedua");
                if(operator == "+"){
                    hasil = num1 + num2;
                }else if(operator == "-"){
                    hasil = num1 - num2;
                }else if(operator == "x" || operator == "*"){
                    hasil = num1 * num2;
                    operator = "x";
                }else if(operator == "/" || operator == ":"){
                    hasil = num1 / num2;
                    operator = ":";
                };
                console.log(`${num1} ${operator} ${num2} = ${hasil}`);
                break;
            case "2":
                num1 = await input1("bilangan radikal");
                hasil = Math.sqrt(num1);
                console.log(`Akar kuadrat dari ${num1} = ${hasil}`);
                break;
            case "3":
                num1 = await input1("panjang sisi");
                hasil = num1**2;
                console.log(`Luas persegi dengan panjang sisi ${num1} = ${hasil}`);
                break;
            case "4":
                num1 = await input1("panjang rusuk");
                hasil = num1**3;
                console.log(`Volume kubus dengan panjang rusuk ${num1} = ${hasil}`);
                break;
            case "5":
                num1 = await input1("panjang rusuk");
                num2 = await input2("tinggi");
                hasil = (Math.PI * (num1**2)) * num2;
                console.log(`Volume tabung dengan jari-jari ${num1} dan tinggi ${num2} = ${hasil}`);
                break;
            case "6":
                break;
            default:
                console.log("Masukkan salah");
        }
        rl.close();
    })
};

main();